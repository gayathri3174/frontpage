import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, Label, Input, FormGroup } from 'reactstrap';

function Login(args) {
  const [modal, setModal] = useState(false);
  const [formdata, setFormdata] = useState({
    email: '',
    password: ''
  })
  //handle the user input 
  const handleInput = (e) => {
    const { name, value } = e.target

    setFormdata({
      ...formdata,
      [name]: value
    })

    console.log(formdata)
  }

  

  const toggle = () => setModal(!modal);


  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("submit is clicked")
    toggle()
    
    
    
  }

  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Login
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label
                for="exampleEmail"
                hidden
              >
                Email
              </Label>
              <Input
                id="exampleEmail"
                name="email"
                placeholder="Email"
                type="email"
                value={formdata.email}
                onChange={handleInput}
                required
              />
            </FormGroup>

            <FormGroup>
              <Label
                for="examplePassword"
                hidden
              >
                Password
              </Label>
              <Input
                id="examplePassword"
                name="password"
                placeholder="Password"
                type="password"
                value={formdata.password}
                onChange={handleInput}
                required
              />
            </FormGroup>
            {' '}
            <Button type='submit' onClick={(e) => handleSubmit(e)} >
              Submit
            </Button>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Login;