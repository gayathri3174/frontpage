import React, { useState } from 'react'
import { Card, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap'

function CardsComponent() {

    const [data, setData] = useState([{
        id: 101,
        title: "watch",
        subtitle: "Discounts",
        discount: '50% discount',
        cardsdiscription: "This is the discription of the watch",
        buttontext: "50%",
        imgurl:"https://m.media-amazon.com/images/I/61+r3+JstZL.jpg"

    },
        {
            id: 102,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the discription of the watch",
            buttontext: "70%",
            imgurl:"https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"

 
        },
        {
            id: 102,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the discription of the watch",
            buttontext: "70%",
            imgurl:"https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"

 
        }]
    )
    return (
        <div>

         
         {
           data.map((item)=>(
            <div key={item.id}>
            <Card
         style={{
             width: '18rem'
         }}
     >
         <img
             alt="Sample"
             src={item.imgurl}
         />
         <CardBody>
             <CardTitle tag="h5">
                 {item.title}
             </CardTitle>
             <CardSubtitle
                 className="mb-2 text-muted"
                 tag="h6"
             >
                 {item.subtitle}
             </CardSubtitle>
             <CardText>
                 {item.cardsdiscription}
             </CardText>
             <Button>
                 {item.buttontext}
             </Button>
         </CardBody>
     </Card>
         </div>

           ))
         }
            

        </div>
    )
}

export default CardsComponent