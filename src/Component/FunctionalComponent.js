import React, { useState } from 'react'

function FunctionalComponent() {
    const [count ,setCount] = useState({
        count :0,
        countValue:1
    });
    
    const  handleIncrement = ()=>{
       
        setCount({
            ...count,
            count:count.count+count.countValue
        })
    }
    const handleDecrement = () =>{
        setCount({
            ...count,
            count:count.count-count.countValue
        })
    }

return(
    <div>
       <h1>{count.count}</h1>
         <button onClick={handleIncrement}>Increment</button>
         <button onClick={handleDecrement}>Decrement</button>
         <input type="number" name = "countValue" value={count.countValue}   onChange={(e) => {
                    setCount({ ...count, countValue: parseInt(e.target.value) });
                }}/>

    </div>
  )

}

export default FunctionalComponent