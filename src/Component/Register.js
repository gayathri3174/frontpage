import React from 'react'
import { Form, FormGroup, Label, Input, Button } from 'reactstrap'
import axios from 'axios'

import { useState } from 'react'

function Register() {
    const [formdata, setFormdata] = useState({
        username: '',
        password: '',
        confirmpassword: ''
    })

    const handleInput = (e) => {
        const { name, value } = e.target

        setFormdata({
            ...formdata,
            [name]: value
        })

        console.log(formdata)
    }


    const saveData = async () => {
        try {
            let response = await axios.post('https://localhost:3005/register', formdata);
            alert('successfull registered');

        } catch (err) {
            console.log(err) 
        }

    }
    const handleSubmit = (e) => {
        e.preventDefault();

        saveData();



    }
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
            <Form onSubmit={handleSubmit} style={{ height: '500px', width: '500px', border: '1px solid', borderRadius: '10px', padding: '10px' }}>
                <FormGroup>
                    <Label
                        for="username"
                        hidden
                    >
                        Email
                    </Label>
                    <Input
                        id="exampleEmail"
                        name="username"
                        placeholder="Username"
                        type="text"
                        value={formdata.username}
                        onChange={handleInput}
                        required
                    />
                </FormGroup>
                {' '}
                <FormGroup>
                    <Label
                        for="examplePassword"
                        hidden
                    >
                        Password
                    </Label>
                    <Input
                        id="examplePassword"
                        name="password"
                        placeholder="Password"
                        type="password"
                        onChange={handleInput}
                        required
                        value={formdata.password}
                    />
                </FormGroup>


                <FormGroup>
                    <Label
                        for="confirmpassword"
                        hidden
                    >
                        Confirm Password
                    </Label>
                    <Input
                        id="confirmpassword"
                        name="confirmpassword"
                        placeholder="conform Password"
                        type="password"
                        onChange={handleInput}
                        required
                        value={formdata.confirmpassword}
                    />
                </FormGroup>



                <Button>
                    Submit
                </Button>
            </Form>
        </div>
    )
}

export default Register