import React from 'react';

class ClassComponent extends React.Component{

    constructor(props) {
      super(props)
    
      this.state = {
        count:0,
        incrementcount :1
      }
    }

    componentDidMount(){
        console.log("Component Mounted");
    }

    componentDidUpdate(){
         console.log("Component is updated")
    }

    handleIncrement = ()=> {

        
        this.setState({count:this.state.count+this.state.incrementcount})

    }

    handleReset = () =>{
        this.setState({count:0})

    }
    handleDecrement = () =>{
        if(this.state.count >0){
            this.setState({count:this.state.count-1})

        }
        
    }

    handleIncrementInput = (e)=> {
        this.setState({incrementcount:parseInt(e.target.value)})

    }
  
    
    render(){
       return (
        <div>
            <h1>Class Component</h1>
            <h1>{this.state.count}</h1>
            <button onClick={this.handleIncrement}>Increment</button>
            <button onClick={this.handleDecrement}>Decrement</button>

            <button onClick={this.handleReset}>Reset</button>

            <input type='number' value={this.state.incrementcount} name="countincrement"   onChange={this.handleIncrementInput}/>

        </div>
       )
    }

    
}
export default ClassComponent;

