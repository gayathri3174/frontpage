import React from 'react'

import './App.css';
import FunctionalComponent from '../src/Component/FunctionalComponent'
import CardComponent from './Component/CardComponent';
import Navigationbar from './Component/Navigationbar';
import Navbar from './Component/NavbarComponent';
import Login from './Component/Login';
import NavbarComponent from './Component/NavbarComponent';
import { Card } from 'reactstrap';
import CardsComponent from './Component/CardsComponent';
import Header from './Component/Header';
import Home from './Component/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoginPage from './Component/LoginPage';
import HomePage from './Component/HomePage';
import LoginComponent from './Component/LoginComponent';
import Register from './Component/Register';
import CardDetails from './Component/CardDetails';
import { Provider } from 'react-redux';
import CounterComponent from './Component/CounterComponent';
import store from './store';

function App() {
  return (
    <div className="App">
      <h1></h1>


      <Provider store={store}>
     <BrowserRouter >
        <Routes>
          <Route path='/' element={<HomePage />}></Route>
          <Route path='/LoginPage' element={<LoginComponent />}></Route>
          <Route path='/Register' element={<Register />}></Route>
          <Route path='/card/:id' element={<CardDetails/>}></Route>
          <Route path='/counter' element={<CounterComponent/>}></Route>
          
           

        </Routes>
      </BrowserRouter>
   


    
     </Provider> 


    </div>
  );
}
export default App;
